from pages.login_page import LoginPage
from tests.base_test import BaseTest
from conftest import read_env_config


class TestLogin(BaseTest):

    def test_correct_login_001(self):
        test_data = read_env_config()
        url = test_data['baseurl']
        username = test_data['username']
        password = test_data['password']
        login_page = LoginPage(self.driver)
        login_page.login(url, username, password)
        login_page.does_url_matches("https://itera-qa.azurewebsites.net/Dashboard")

    def test_incorrect_login_002(self):
        test_data = read_env_config()
        url = test_data['baseurl']
        login_page = LoginPage(self.driver)
        login_page.login(url, 'xyz', 'xyz')
        assert login_page.is_incorrect_login_alert_displayed()
