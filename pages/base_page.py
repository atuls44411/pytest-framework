from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage:

    def __init__(self, driver):
        self.driver = driver
        self.wait = WebDriverWait(self.driver, 30)

    def go_to_url(self, url):
        self.driver.get(url)

    def find_element(self, locator):
        el = self.wait.until(EC.presence_of_element_located(locator))
        return el

    def does_url_matches(self, url):
        return self.driver.current_url == url

    def is_element_displayed(self, locator):
        return self.wait.until(EC.presence_of_element_located(locator))
