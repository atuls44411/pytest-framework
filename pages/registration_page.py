from selenium.webdriver.common.by import By

from pages.base_page import BasePage


class RegistrationPage(BasePage):

    PAGE_URL = "https://itera-qa.azurewebsites.net/UserRegister/NewUser"
    USER_FIRST_NAME = By.ID, "FirstName"
    USER_LAST_NAME = By.ID, "Surname"
    USER_E_POST = By.ID, "E_post"
    USER_MOBILE = By.ID, "Mobile"
    USER_USERNAME = By.ID, "Username"
    USER_PASSWORD = By.ID, "Password"
    USER_CONFIRM_PASSWORD = By.ID, "ConfirmPassword"
    SUBMIT_BUTTON = By.ID, "submit"
    REGISTRATION_SUCCESSFUL_MSG = By.XPATH, "//label[text()='Registration Successful']"

    def go_to_page(self):
        self.go_to_url(self.PAGE_URL)

    def enter_firstname(self, firstname):
        self.find_element(self.USER_FIRST_NAME).send_keys(firstname)

    def enter_lastname(self, lastname):
        self.find_element(self.USER_LAST_NAME).send_keys(lastname)

    def enter_epost(self, epost):
        self.find_element(self.USER_E_POST).send_keys(epost)

    def enter_mobile(self, mobile):
        self.find_element(self.USER_MOBILE).send_keys(mobile)

    def enter_username(self, username):
        self.find_element(self.USER_USERNAME).send_keys(username)

    def enter_password(self, password):
        self.find_element(self.USER_PASSWORD).send_keys(password)

    def enter_confirm_password(self, confirm_password):
        self.find_element(self.USER_CONFIRM_PASSWORD).send_keys(confirm_password)

    def click_submit(self):
        self.find_element(self.SUBMIT_BUTTON).click()

    def perform_new_user_registration(self, user_data):
        self.enter_firstname(user_data['firstname'])
        self.enter_lastname(user_data['lastname'])
        self.enter_epost(user_data['epost'])
        self.enter_mobile(user_data['mobile'])
        self.enter_username(user_data['username'])
        self.enter_password(user_data['password'])
        self.enter_confirm_password(user_data['password'])
        self.click_submit()

    def is_registration_successful_msg_displayed(self):
        return self.is_element_displayed(self.REGISTRATION_SUCCESSFUL_MSG)
