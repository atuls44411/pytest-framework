import os

from pages.registration_page import RegistrationPage
from tests.base_test import BaseTest
from utils import file_helper, datetime_helper


class TestRegistration(BaseTest):

    def test_user_registration(self):
        test_data_file_path = os.path.dirname(__file__) + "/test_data.json"
        test_data = file_helper.get_test_data(test_data_file_path)
        current_time = datetime_helper.get_current_timestamp()
        registration_page = RegistrationPage(self.driver)
        for key, value in test_data['new_user_registration'].items():
            test_data['new_user_registration'].update({key: value+str(current_time)})
        registration_page.go_to_page()
        registration_page.perform_new_user_registration(test_data['new_user_registration'])
        assert registration_page.is_registration_successful_msg_displayed()
