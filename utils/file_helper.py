import json


def get_test_data(path):
    with open(path, 'r') as f:
        data = json.loads(f.read())
    return data
