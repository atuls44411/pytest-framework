import datetime


def get_current_timestamp():
    ct = datetime.datetime.now().strftime("_%d_%m_%Y_%H_%M_%S")
    return ct
