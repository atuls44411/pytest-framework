import json
import os

import pytest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager


@pytest.fixture(scope="class")
def create_driver(request):
    global master_config_data, driver
    master_config_data = read_master_config()
    if master_config_data['browser'] == 'chrome':
        driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
    elif master_config_data['browser'] == 'firefox':
        driver = webdriver.Chrome(service=Service(GeckoDriverManager().install()))
    request.cls.driver = driver
    driver.implicitly_wait(30)
    driver.maximize_window()
    yield
    driver.quit()


def read_master_config():
    master_config_path = os.path.dirname(__file__) + "/config/master_config.json"
    with open(master_config_path, 'r') as f:
        data = json.loads(f.read())
    return data


def read_env_config():
    global env_config_path
    if master_config_data['env'] == 'prod':
        env_config_path = os.path.dirname(__file__) + "/config/prod_env_config/config.json"
    elif master_config_data['env'] == 'qa':
        env_config_path = os.path.dirname(__file__) + "/config/qa_env_config/config.json"
    with open(env_config_path, 'r') as f:
        data = json.loads(f.read())
    return data
