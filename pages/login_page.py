from selenium.webdriver.common.by import By

from pages.base_page import BasePage


class LoginPage(BasePage):

    LOGIN_BUTTON = By.XPATH, "//a[text()='Login']"
    USERNAME_TEXTBOX = By.ID, "Username"
    PASSWORD_TEXTBOX = By.ID, "Password"
    LOGIN_SUBMIT_BUTTON = By.NAME, "login"
    INCORRECT_LOGIN_ALERT = By.XPATH, "//label[text()='Wrong username or password']"

    def click_login_button(self):
        self.find_element(self.LOGIN_BUTTON).click()

    def enter_username(self, username):
        self.find_element(self.USERNAME_TEXTBOX).send_keys(username)

    def enter_password(self, password):
        self.find_element(self.PASSWORD_TEXTBOX).send_keys(password)

    def click_login_submit(self):
        self.find_element(self.LOGIN_SUBMIT_BUTTON).click()

    def login(self, url, username, password):
        self.driver.get(url)
        self.click_login_button()
        self.enter_username(username)
        self.enter_password(password)
        self.click_login_submit()

    def is_incorrect_login_alert_displayed(self):
        return self.is_element_displayed(self.INCORRECT_LOGIN_ALERT)
